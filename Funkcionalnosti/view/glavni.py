'''
Created on May 2, 2019

@author: Milica
'''
from view.util import prikaz_menija, provera_unosa
from view.sekcija import meni_sekcija
from view.polica import meni_polica

class glavni_meni():
    while True:
        prikaz_menija("Koju opciju zelite","1-Sekcije","2-Police","3-Izlaz")
        opcija=provera_unosa("Opcija:","Opcija mora biti ceo broj",int)
        if opcija==3:
            break
        elif opcija==1:
            meni_sekcija()
        elif opcija==2:
            meni_polica()
if __name__=="__main__":
    pass


    